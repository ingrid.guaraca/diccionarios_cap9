#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Escribir un programa que clasifica cada mensaje de correo dependiendo del día de la semana en que se recibió.
# Para hacer esto busca las líneas que comienzan con “From”, después busca por la tercer palabra y mantén un
# contador para cada uno de los días de la semana. Al final del programa imprime los contenidos de tu diccionario
# (el orden no importa).


archivo = input("Ingresa un nombre de archivo: ")
dia = {}

with open(archivo) as arch:
    for l in arch:
        if l.startswith("From "):
            word  = l.split(maxsplit=3)[2]
            if word in ("Mon", "Tue", "Wed" , "Thu", "Fri", "Sat", "Sun"):
                dia[word] = dia.get(word, 0) + 1
print(dia)
