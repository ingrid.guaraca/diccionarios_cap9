#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Agrega código al programa anterior para determinar quién tiene la mayoría de mensajes en el archivo. Después de que
# todos los datos hayan sido leídos y el diccionario haya sido creado, mira a través del diccionario utilizando un
# bucle máximo (ve Capítulo 5: Bucles máximos y mínimos) para encontrar quién tiene la mayoría de mensajes e imprimir
# cuántos mensajes tiene esa persona.

archivo = input("Ingresa un nombre de archivo: ")
arch = open(archivo)
counts = dict()
for l in arch:
    l.rstrip()
    if not l.startswith("From "): continue
    words = l.split()
    counts[words[1]] = counts.get(words[1],0)+1
maxi = None
maxi_sms = None
for key in counts:
    if maxi is None: maxi = counts[key]
    if maxi < counts[key]:
        maxi = counts[key]
        maxi_sms = key
print(maxi_sms, maxi)
