#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Programa para leer a través de un historial de correos, construye un histograma utilizando un
# diccionario para contar cuántos mensajes han llegado de cada dirección de correo electrónico, e imprime el
# diccionario.

archivo = input("Ingresa un nombre de archivo: ")
arch = open(archivo)
counts = dict()
for l in arch:
    l.rstrip()
    if not l.startswith("From "): continue
    words = l.split()
    counts[words[1]] = counts.get(words[1],0)+1
print(counts)


