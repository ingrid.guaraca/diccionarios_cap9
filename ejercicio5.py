#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Este programa almacena el nombre del dominio (en vez de la dirección) desde donde fue enviado el mensaje en vez de
# quién envió el mensaje (es decir, la dirección de correo electrónica completa). Al final del programa, imprime el
# contenido de tu diccionario.

archivo = input("Ingresa un nombre de archivo: ")
counts = dict()
try:
    arch = open(archivo)
except:
    print("No se puede abrir el archivo", archivo)
    exit()
for l in arch:
    words = l.split()
    if len(words) < 2 or words[0] != "From":
        continue
    else:
        dir = words[1].find("@")
        dominio = words[1][dir+1:]
        if dominio not in counts:
            counts[dominio] = 1
        else:
            counts[dominio] += 1
print(counts)