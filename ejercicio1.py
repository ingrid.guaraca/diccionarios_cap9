#__autora__ : __"Ingrid Guaraca"__
#__email__ : __"ingrid.guaraca@unl.edu.ec"__
# Descargar una copia del archivo www.py4e.com/code3/words.txt. Escribe un programa que lee las palabras en words.txt
# y las almacena como claves en un diccionario. No importa qué valores tenga. Luego puedes utilizar el operador in
# como una forma rápida de revisar si una cadena está en el diccionario.

archivo = input("Ingresa el nombre del archivo: ")
try:
    arch = open(archivo)
except:
    print("No se puede abrir el archivo", archivo)
    exit()
counts = dict()
for l in arch:
    words = l.split()
    for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1
print(counts)


